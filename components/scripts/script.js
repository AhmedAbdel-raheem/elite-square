
(function ($) {
    

    function textBlockCentering() {
        // Start float title rotate
        var block = jQuery('#slider .text-block');

        var result = jQuery('section').find(block);
        var section = result.parents('section');

        var diff = (section.height() - result.height()) / 2;
        result.css('top', diff);
        // coverBlockCenter();
        // sliderBlockCenter();
        // result.css('bottom', bottom+'px')
        // End float title rotate
    }


    function coverCanvas(canvas, canvasApply, $) {
        for (let i = 0; i < canvas.length; i++) {
            const element = canvas[i];
            var coverHeight = $("#slider").height();
            canvasApply(element, $(window).width(), coverHeight);
        }
    }

    $(document).ready(function () {
        var canvas = document.getElementsByClassName('cover-canvas');
        coverCanvas(canvas, canvasApply, $);
        textBlockCentering()
        // Carousel for partners logos on homepage
        // Start Owl Carousel
        owlCarouselInit($);
        // End Owl Carousel
        // Start Navbar Dropdown
        $('.dropdown-toggle').on('click', function (e) {
            e.preventDefault()
        })
        // End Navbar Dropdown

        $(window).resize(function () {
        coverCanvas(canvas, canvasApply, $);

        });
        $(document).on('click', '.btn-search', function (e) {
            e.preventDefault();
            var parent = $(this).parents('.btn-container');
            parent.toggleClass('show-search')

        })

        $('.search-form #price').ionRangeSlider({
            type: 'double',
            skin: "flat",
            grid: false,
            min: 0,
            max: 1000000,
            from: 21,
            postfix: " L.E."
        });

        $('.search-form #area').ionRangeSlider({
            type: 'double',
            skin: "flat",
            grid: false,
            min: 80,
            max: 8000,
            from: 80,
            postfix: " m<sup>2</sup>"
        });

        let items = $('.developer-item');
        if (items.length == 1) {
            items.clone().appendTo('#carousel')
            items.clone().appendTo('#carousel')
            items.clone().appendTo('#carousel')
        } else if (1 < items.length <= 3) {
            items.clone().appendTo('#carousel')
        }

        // Caoursel 2
        featuredCarouselInit($);

        // Slick Carousel change center on click
        slickCarouselChange($);

    })




})(jQuery);
function owlCarouselInit($) {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop: true,
        // margin: 40,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                loop: true,
                // nav: false,
            },
            600: {
                items: 2,
                loop: true,
                // nav: false,
            },
            768: {
                items: 3,
                loop: true,
                // nav: false,
            },
            1000: {
                items: 4,
                loop: true,
                // nav: false,
                // loop: false
            }
        }
    });
    // Go to the next item
    $('.customNextBtn').click(function (e) {
        e.preventDefault();
        owl.trigger('next.owl.carousel');
    });
    // Go to the previous item
    $('.customPrevBtn').click(function (e) {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        e.preventDefault();
        owl.trigger('prev.owl.carousel', [300]);
    });
}

function featuredCarouselInit($) {
    $('.carousel-2').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        infinite: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });
}

function slickCarouselChange($) {
    $(document).on('click', '.developer-item.slick-slide', function (e) {
        e.preventDefault();
        if ($(this).prev().hasClass('slick-center')) {
            $("#carousel").slick('slickNext');
        }
        else if ($(this).next().hasClass('slick-center')) {
            $("#carousel").slick('slickPrev');
        }
    });
}

function canvasApply(canvas, windowWidth, windowHeight) {


    var context = canvas.getContext('2d');

    // Image Object init
    var imageObj = new Image();
    imageObj.src = '../assets/' + canvas.getAttribute('imgSrc');
    // Canavas dimensions
    canvas.width = windowWidth;
    canvas.height = windowHeight;
    var fitImageOn = function (context, img, x, y, w, h, offsetX, offsetY) {

        if (arguments.length === 2) {
            x = y = 0;
            w = context.canvas.width;
            h = context.canvas.height;
        }

        // default offset is center
        offsetX = typeof offsetX === "number" ? offsetX : 0.5;
        offsetY = typeof offsetY === "number" ? offsetY : 0.5;

        // keep bounds [0.0, 1.0]
        if (offsetX < 0) offsetX = 0;
        if (offsetY < 0) offsetY = 0;
        if (offsetX > 1) offsetX = 1;
        if (offsetY > 1) offsetY = 1;

        var iw = img.width,
            ih = img.height,
            r = Math.min(w / iw, h / ih),
            nw = iw * r,   // new prop. width
            nh = ih * r,   // new prop. height
            cx, cy, cw, ch, ar = 1;

        // decide which gap to fill    
        if (nw < w) ar = w / nw;
        if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;  // updated
        nw *= ar;
        nh *= ar;

        // calc source rectangle
        cw = iw / (nw / w);
        ch = ih / (nh / h);

        cx = (iw - cw) * offsetX;
        cy = (ih - ch) * offsetY;

        // make sure source rectangle is valid
        if (cx < 0) cx = 0;
        if (cy < 0) cy = 0;
        if (cw > iw) cw = iw;
        if (ch > ih) ch = ih;

        // fill image in dest. rectangle
        context.drawImage(img, cx, cy, cw, ch, x, y, w, h);
    };

    imageObj.onload = function () {
        fitImageOn(context, imageObj, 0, 0, canvas.width, canvas.height);
    };
    // textBlockCentering();


}

